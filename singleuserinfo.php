<?php
if (isset($_GET) && !empty($_GET)) {
    $user_id = $_GET['user_id'];
    include 'db.php';
    $sql = "SELECT * FROM user WHERE id=$user_id";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $arr = array(
                'username' => $row['name'],
                'dob' => date("d/m/Y", strtotime($row['dob'])),
                'email' => $row['email'],
                'address' => $row['address'],
                'income' => $row['income'],
            );
        }
            echo json_encode($arr);
    } else {
        echo "false";
    }
}

?>