<?php 
include 'db.php';
if (isset($_POST) && !empty($_POST)) {
	$name = $_POST['name'];
	$dob = $_POST['dob'];
	$email = $_POST['email'];
	$address = $_POST['address'];
	$income = $_POST['income'];
	if ($name != '' && $dob != '' && $email != '' && $address != '' && $income != '') {
		$dob = date("Y-m-d", strtotime($dob) );
		$sql = "INSERT INTO user (name, dob, email,address,income)
		VALUES ('$name','$dob','$email','$address','$income')";

		if (mysqli_query($conn, $sql)) {
		    header("Location: index.php?user=added");
		}
	}
}
else {
	$name = '';
	$dob = '';
	$email = '';
	$address = '';
	$income = '';
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add User</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
		li { cursor: pointer; }
	</style>
</head>
<body>

	<div class="container">
	  <h2>User</h2>

	  <form method="POST">
	  	<?php if (isset($_POST) && !empty($_POST)) { ?>
	  		<div class="alert alert-danger">
			  *All Fields are required
			</div>
		<?php } ?>
	    <div class="form-group">
	      <label for="name">Name:</label>
          <input type="text" name="name" value="<?= $name ?>" class="form-control" autocomplete ="off" required="required" placeholder="Enter Name">
	    </div>
	    <div class="form-group">
			<label>Email</label>
		    <input type="email" name="email" id="email" value="<?= $email ?>" class="form-control" placeholder="Enter Email" required="required">
		</div>
	    <div class="form-group">
			<label>Date Of Birth</label>
		    <input type="text" id="datepicker" id="dob" readonly value="<?= $dob ?>" name="dob" class="form-control" placeholder="DD/MM/YYYY" required="required">
		</div>
		
	    <div class="form-group">
		  <label for="comment">Address:</label>
		  <textarea class="form-control" id="address" name="address" value="<?= $address ?>" rows="5" required="required"></textarea>
		</div>
		<div class="form-group">
			<label>Income</label>
		    <input type="text" name="income" id="income" value="<?= $income ?>" class="allow_decimal form-control" required="required">
		    <span style="font-size: 12px;">*Allow numeric values with decimal</span>
		</div>
	    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
	    <a href="index.php"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to list</button></a>
	  </form>
	</div>
<script type="text/javascript">
	$(function() {
	    $( "#datepicker").datepicker({
	        format: "dd/mm/yyyy",
	        changeMonth : true,
	        changeYear : true,
	        autoclose: true,
	        endDate: "<?= date('d/m/Y') ?>",
	    });
	});
	$(".allow_decimal").on("input", function(evt) {
	   var self = $(this);
	   self.val(self.val().replace(/[^0-9\.]/g, ''));
	   if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
	   {
	     evt.preventDefault();
	   }
	 });
</script>
</body>
</html>