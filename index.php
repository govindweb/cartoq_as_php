<?php 
include 'db.php';
$sql = "SELECT * FROM user";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title>User Detail</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
</head>
<body>

	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<h2>Users List</h2>
				<a href="add-user.php"><button type="button" class="btn btn-info"><i class="fa fa-user"></i> Add User</button></a>&nbsp;&nbsp;<a href="detail-listing.php"><button type="button" class="btn btn-primary"><i class="fa fa-reorder"></i> User Details</button></a>
				<?php if ($_GET['user'] == 'added') { ?>
					<div class="alert alert-success" style="margin-top: 20px;">
				  <strong>Success!</strong> User added successfully.
				</div>
				<?php } ?>
			</div>

		</div>
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-12">
		  <table id="example" class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>DOB</th>
	                <th>Email</th>
	                <th>Address</th>
	                <th>Income</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php if ($result->num_rows > 0) { 
	        		while($row = $result->fetch_assoc()) {
	        			?>
	            <tr>
	                <td><?= $row['name'] ?></td>
	                <td><?= date("d/m/Y", strtotime($row['dob']) ) ?></td>
	                <td><?= $row['email'] ?></td>
	                <td><?= $row['address'] ?></td>
	                <td><?= $row['income'] ?></td>
	            </tr>
	        	<?php } } else { echo 'No Result Found'; }?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>